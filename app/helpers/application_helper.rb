module ApplicationHelper
    def is_active_controller(controller_name)
        params[:controller] == controller_name ? "active" : nil
    end

    def is_active_action(action_name)
        params[:action] == action_name ? "active" : nil
    end

    def department_path department
    	"/departments/#{department.id}/#{department.display_name}".gsub!(' ', '_')
    end

    def render_potential_matches matches
      output = ""

      matches.each do |match|
        output += "<a href='#{match[:url]}'>#{match[:contract_number]}</a>, "
      end

      return output[0..output.length-3]
    end
end
require 'chart'
class ChartsController < ApplicationController
  def show

    @chart = Chart.new params
    render json: @chart.data
  end

  def chart_params
    params.permit(:entity_type, :entity_id, :chart_type, :chart_year)
  end
end
class DepartmentsController < ApplicationController
  def show
  	@department = Department.find(params[:id])
    @purchases = @department.purchases.where(potential_epp: true).order(purchase_date: :desc)
  end

  def index
    @departments = Department.all
  end

  def line_graph_data
    @dept = Department.find params[:id]
    data_points = []

    for i in 2012..Date.today.year
      data_points << { 
        year: i,
        value: @dept.epp_percent_by_year(i)
      }
    end

    render :json => data_points

  end
end
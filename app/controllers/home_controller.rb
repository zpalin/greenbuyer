require 'Summary'

class HomeController < ApplicationController
  def index
    @summary = Summary.new
    @epp_spending_leaders = Department.unscoped.order(epp_percent_cache: :desc).limit(5)
  end

  def minor
  end

  def coming_soon
  end
end

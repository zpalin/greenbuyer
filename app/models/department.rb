class Department < ActiveRecord::Base
	default_scope {order('display_name ASC')}

  def purchases
    @purchases ||= Purchase.where(department_name: name)
  end

  def total_spending
    Rails.cache.fetch("#{id}/total_spending") do
      purchases.sum(:total_price)
    end
  end

  def non_epp_spending
    total_spending - epp_spending - potential_epp_spending
  end

  def potential_epp_spending
    Rails.cache.fetch("#{id}/potential_epp_spending") do
      purchases.where(potential_epp: true).sum(:total_price)
    end
  end

  def epp_spending
    Rails.cache.fetch("#{id}/epp_spending") do
      purchases.where(epp_compliant: true).sum(:total_price)
    end
  end

  def epp_percent
    (epp_spending / total_spending) * 100
  end

  def potential_epp_percent
    (potential_epp_spending / total_spending) * 100
  end

  def sabrc_spending
    Rails.cache.fetch("#{id}/sabrc_spending") do
      purchases.where(sabrc_compliant: true).sum(:total_price)
    end
  end

  def sabrc_percent
    (sabrc_spending / total_spending) * 100
  end

  def total_spending_by_year(year)
    Rails.cache.fetch("#{id}/#{year}/total_spending_by_year") do
      purchases.for_year(year).sum(:total_price)
    end
  end

  def non_epp_spending_by_year(year)
    total_spending_by_year(year) - epp_spending_by_year(year) - potential_epp_spending_by_year(year)
  end

  def epp_spending_by_year(year)
    Rails.cache.fetch("#{id}/#{year}/epp_spending_by_year") do
      purchases.for_year(year).where(epp_compliant: true).sum(:total_price)
    end
  end

  def epp_percent_by_year(year)
    (epp_spending_by_year(year) / total_spending_by_year(year)) * 100
  end

  def sabrc_spending_by_year(year)
    Rails.cache.fetch("#{id}/#{year}/sabrc_spending_by_year") do
      purchases.for_year(year).where(sabrc_compliant: true).sum(:total_price)
    end
  end

  def sabrc_percent_by_year(year)
    (sabrc_spending_by_year(year) / total_spending_by_year(year)) * 100
  end

  def potential_epp_spending_by_year(year)
    Rails.cache.fetch("#{id}/#{year}/potential_epp_spending_by_year") do
      purchases.for_year(year).where(potential_epp: true).sum(:total_price)
    end
  end

end

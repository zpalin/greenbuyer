class ContractPurchaseRelation < ActiveRecord::Base
  belongs_to :contract
  belongs_to :purchase
end

class Purchase < ActiveRecord::Base
  has_many :contract_purchase_relations
	scope :for_year, lambda {|year| where("purchase_date >= ? and purchase_date <= ?", "#{year}-01-01", "#{year}-12-31")}

  def department
    @department ||= Department.where(name: department_name).first
  end

  def contract_url
    Contract.where(contract_number: potential_contract).first.url rescue ""
  end

  def potential_contract_matches
    Rails.cache.fetch("#{id}/potential_contract_matches") do
      get_potential_contract_matches
    end
  end



  def get_potential_contract_matches
    matches = []
    contract_purchase_relations.each do |match|
      matches << { 
        contract_number: match.contract.contract_number, 
        url: match.contract.url
      }
    end
    return matches
  end
  private :get_potential_contract_matches
end

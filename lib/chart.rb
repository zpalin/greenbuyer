require 'Summary'

class Chart
  def initialize(args)
    @entity_type = args[:entity_type] # SUMMARY OR DEPARTMENT
    @entity_id = args[:entity_id]     # DEPARTMENT ID
    @chart_type = args[:chart_type]   # DONUT OR LINE
    @chart_year = args[:chart_year]   # ALL OR YEAR NUMBER
  end

  def data
    return case @chart_type.upcase
    when "DONUT"
      generate_donut
    when "LINE"
      generate_line
    else
      { error: "Invalid Chart Type." }
    end
  end



  # LINE CHART LOGIC
  def generate_donut
    return case @entity_type.upcase
    when "SUMMARY"
      summary_donut
    when "DEPARTMENT"
      department_donut
    else
      { error: "Invalid Entity Type." }
    end
  end

  def summary_donut
    summary = Summary.new

    return case @chart_year.upcase
    when "ALL"
      [
        { label: "Non-EPP Purchases",       value: summary.non_epp_spending },
        { label: "EPP Purchases",           value: summary.epp_spending },
        { label: "Potential EPP Purchases", value: summary.potential_epp_spending }
      ]
    else
      [
        { label: "Non-EPP Purchases",       value: summary.non_epp_spending_by_year(@chart_year) },
        { label: "EPP Purchases",           value: summary.epp_spending_by_year(@chart_year) },
        { label: "Potential EPP Purchases", value: summary.potential_epp_spending_by_year(@chart_year) }
      ]
    end

  end

  def department_donut
    department = Department.find @entity_id rescue nil
    if department.nil? then return { error: "Invalid Department Id" } end

    return case @chart_year.upcase
    when "ALL"
      [
        { label: "Non-EPP Purchases",       value: department.non_epp_spending },
        { label: "EPP Purchases",           value: department.epp_spending },
        { label: "Potential EPP Purchases", value: department.potential_epp_spending }
      ]
    else
      [
        { label: "Non-EPP Purchases",       value: department.non_epp_spending_by_year(@chart_year) },
        { label: "EPP Purchases",           value: department.epp_spending_by_year(@chart_year) },
        { label: "Potential EPP Purchases", value: department.potential_epp_spending_by_year(@chart_year) }
      ]
    end

  end

  # DONUT CHART LOGIC
  def generate_line
    return case @entity_type.upcase
    when "SUMMARY"
      summary_line
    when "DEPARTMENT"
      department_line
    else
      { error: "Invalid Entity Type." }
    end
  end  

  def summary_line
    summary = Summary.new

    data_points = []
    for i in 2012..Date.today.year
      data_points << { 
        year: i,
        value: summary.epp_percent_by_year(i)
      }
    end
    return data_points
  end

  def department_line
    department = Department.find @entity_id rescue nil
    if department.nil? then return { error: "Invalid Department Id" } end

    data_points = []
    for i in 2012..Date.today.year
      data_points << { 
        year: i,
        value: department.epp_percent_by_year(i)
      }
    end
    return data_points
  end

end
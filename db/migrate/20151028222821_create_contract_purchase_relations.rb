class CreateContractPurchaseRelations < ActiveRecord::Migration
  def change
    create_table :contract_purchase_relations do |t|
      t.integer :contract_id
      t.integer :purchase_id
      t.string :contract_unspsc_match
      t.string :purchase_unspsc_match

      t.timestamps null: false
    end
  end
end

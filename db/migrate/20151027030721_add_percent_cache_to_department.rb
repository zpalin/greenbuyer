class AddPercentCacheToDepartment < ActiveRecord::Migration
  def change
    add_column :departments, :epp_percent_cache, :decimal
  end
end
